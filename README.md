---
TODO list and INSTALL
---
# Install
##Requires
 + Alacritty
 + dmenu
 + hsetroot -root -fill <wallpaper>
 + xscreensaver (to lock the screen)
 + requires aRandR to generate a /.screenlayout/dualscreen.sh:

# TODO
- Bindings
   - Alt-tab: list all windows of the workspace
   - Super-tab: list all windows
   - Super - Enter:  terminal centered
   - Super-ctrl-r: restart 
   - alt-space: dmenu/ ROFI?
   - BUGSuper-left: move window on the left 
   - BUGSuper-right: move window on the right
   - BUGSuper-up: move window up
   - BUGSuper-down: move window down
   - Ctrl-left: shrink window on the left 
   - Ctrl-right: grow window on the right
   - Ctrl-up: shrink window up
   - Ctrl-down: grow window down
   - Super-m: maximize window
   - Super-n: minimize window
   - Super-F1: go to workspace 1
   - Super-F2: go to workspace 2
   - Super-F3: go to workspace 3
   - Super-F4: go to workspace 4
   - Super-L: lock screen
   - ?Ctrl+Esc: drop down terminal
   - Super + left click: move window
- little fvwm module (top right) with:
   - time/date
   - stalonetray
   - Pager?
   - Window list ?
- ZSH config
   - alias
   - prompt
- .Xresource
   - terminal theme
- Menu redesign

# BUGS:

 

